#include  "Store.h"
#include <iostream>
#include <sstream>
/*
Store.cpp
MISIS: M00734828
Created: 7/1/2021
Updated: 15/1/2021
*/

/*
  getter for product name
  @return products name
*/
std::string Store::getProductName()
{
    return product_name;
}

/*
  getter for product id
  @return products id
*/
int Store::getId()
{
    return product_id;
}

/*
  getter for product price
  @return products price
*/
double Store::getPrice()
{
    return price;
}

/*
  setter for product quantity
  @param quantity the quantity entered by the user
*/
void Store::setQuantity(int quantity)
{
    this->quantity = quantity;
}

/*
  getter for product quantity
  @return products quantity
*/
int Store::getQuantity()
{
    return quantity;
}

