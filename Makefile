CXX = g++
CXXFLAGS = -g -Wall -Wextra

program : Store.cpp Sale.cpp main.cpp
	$(CXX) $(CXXFLAGS) -o program Store.cpp Sale.cpp main.cpp

run : program
	./program

clean :
	$(RM) *.o
	$(RM) program
