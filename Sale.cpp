#include "Sale.h"
#include <iostream>
#include <ctime>
/*
Sale.cpp
MISIS: M00734828
Created: 7/1/2021
Updated: 15/1/2021
*/

/*
 function used to set the sales set_sales_data
 @param it, it is an object of class Store
 @param item_quantity, quantity sold by the Store
 @param sale_data_time, displays the system time
*/
void Sale::set_sales_data(Store it, int item_quantity)
{
  store = it;
  sales_quantity = item_quantity;
  time(&sale_date_time);
}

/*
 function used to display the sales data on the command line.
*/
void Sale::display_info()
{ 
  std::cout << "\tDate|time: " << asctime(localtime(&sale_date_time));
  std::cout << "\tItem id: " << store.getId() << std::endl;
  std::cout << "\tItem name: " << store.getProductName() << std::endl;
  std::cout << "\tItem Price: $" << store.getPrice() << std::endl;std::cout << "\tQuantity: " << sales_quantity << std::endl;
  std::cout << "\tTotal price: $" << (store.getPrice()*sales_quantity) << std::endl;
  std::cout << "\n\n\t\t\t---------------------------------------------\n";
}