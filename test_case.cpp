// To run the program use g++ -o tests test_case.cpp Store.cpp Sale.cpp

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <string.h>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "Sale.h"
#include "Books.h"
#include "Cd.h"
#include "Dvd.h"
#include "Magazines.h"
#include "Store.h"
#include <vector>
/*
main.cpp
MISIS: M00734828
Created: 8/1/2021
Updated: 15/1/2021
*/

/*
 function used to sell items from their respective files
*/
int sellItems(){
  Store store_obj; //object of parent class
  int flag = 0;   //used to flag the presence of a data
  std::cout << "\n\nSELL SUB-MENU: " << std::endl;
  std::cout << "1) CD\n";
  std::cout << "2) DVD\n";
  std::cout << "3) Magazine\n";
  std::cout << "4) Book\n";
  int type;
  std::cin >> type;  //to select the type of product required by the user
  switch(type) {     //switch case is used to switch between products according to the users choice
    case 1:{
      std::fstream cd_file;
      std::cout << "\nEnter id: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      cd_file.open("cd.txt", std::ios::in | std::ios::out);          //open the cd.txt file in default mode
      while (cd_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName())) {             //if statement to check if the mentioned ID and is present in the file
        std::cout << std::endl << "Enter required quantity: " <<"\n";
        int quantity;
        std::cin >> quantity;
        if (quantity <= store_obj.getQuantity()) {   //if statement to check if the mentioned quantity is less than the available stock
          store_obj.setQuantity(store_obj.getQuantity() - quantity);
          unsigned long int position = 0;
          position = -1 * sizeof(Store);            //to seek through the line to find the required item or its attributes
          cd_file.seekp(position, std::ios::cur);
          cd_file.write((char *) &store_obj, sizeof(store_obj));  //changes the quantity
          flag = 1;                               
          Sale sale_obj;
          std::ofstream file1("sales_information.txt", std::ios::binary | std::ios::app); //opens a new text file and writes the sold item in it
          if (!file1) {
            std::cout << "FILE NOT FOUND\n";
            return 1;
          }
          sale_obj.set_sales_data(store_obj, quantity);
          file1.write((char *) &sale_obj, sizeof(sale_obj));
          file1.close();
          cd_file.close();
          std::cout << "\nSale successful\n";
          } 
          else {                     //ele statement if the quantity is not availabe
            std::cout << "The required quantity is not available"<< std::endl;
            flag = 1;
          }
      break;
      }
      }
      if (flag == 0) {            
        cd_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      cd_file.close();
    }
    break;
    case 2: 
    {
      std::fstream dvd_file;
      std::cout << "\nEnter DVD id: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      dvd_file.open("dvd.txt", std::ios::in | std::ios::out);          //open the dvd.txt file in default mode
      while (dvd_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName())) {             //if statement to check if the mentioned ID and is present in the file
          std::cout << std::endl << "Enter required quantity: " << "\n";
          int quantity;
          std::cin >> quantity;
          if (quantity <= store_obj.getQuantity()) {    //if statement to check if the mentioned quantity is less than the available stock
            store_obj.setQuantity(store_obj.getQuantity() - quantity);
            unsigned long int position = 0;
            position = -1 * sizeof(Store);   //to seek through the line to find the required item or its attributes
            dvd_file.seekp(position, std::ios::cur);
            dvd_file.write((char *) &store_obj, sizeof(store_obj));   //changes the quantity
            flag = 1;
            Sale sale_obj;
            std::ofstream file1("sales_information.txt", std::ios::binary | std::ios::app); //opens a new text file and writes the sold item in it
            if (!file1) {
              std::cout << "FILE NOT DOUND\n";
              return 1;
            }
            sale_obj.set_sales_data(store_obj, quantity);
            file1.write((char *) &sale_obj, sizeof(sale_obj));
            file1.close();
            dvd_file.close();
            std::cout << "\nSale successful\n";
          } 
          else 
          {
          std::cout << "The required quantity is not available"<< std::endl;
          flag = 1;
          }
        break;
        }
      }
      if(flag == 0)
      {
      dvd_file.close();
      std::cout << "\nProduct doesn't exist" << std::endl;
      }
      dvd_file.close();
    }
    break;
    case 3: 
    {
      std::fstream mag_file;
      std::cout << "\nEnter magazine id: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      mag_file.open("magazine.txt", std::ios::in | std::ios::out);          //open the magazine.txt file in default mode
      while (mag_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName())) {             //if statement to check if the mentioned ID and is present in the file
          std::cout << std::endl << "Enter purchase quantity: " << "\n";
          int quantity;
          std::cin >> quantity;
          if (quantity <= store_obj.getQuantity()) {  //if statement to check if the mentioned quantity is less than the available stock
            store_obj.setQuantity(store_obj.getQuantity() - quantity);
            unsigned long int position = 0;
            position = -1 * sizeof(Store);  //to seek through the line to find the required item or its attributes
            mag_file.seekp(position, std::ios::cur);
            mag_file.write((char *) &store_obj, sizeof(store_obj));  //changes the quantity
            flag = 1;
            Sale sale_obj;
            std::ofstream file1("sales_information.txt", std::ios::binary | std::ios::app); //opens a new text file and writes the sold item in it
            if (!file1) {
              std::cout << "FILE NOT FOUND\n";
              return 1;
            }
            sale_obj.set_sales_data(store_obj, quantity);
            file1.write((char *) &sale_obj, sizeof(sale_obj));
            file1.close();
            mag_file.close();
            std::cout << "\nSale successful\n";
          } 
          else {
            std::cout << "The required quantity is not available"<< std::endl;
            flag = 1;
          }
        break;
        }
      }
      if(flag == 0){
        mag_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      mag_file.close();
    }
    break;
    case 4:
    {
      std::fstream book_file;
      std::cout << "\nEnter book id: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      book_file.open("book.txt", std::ios::in | std::ios::out);          //open the book.txt file in default mode
      while (book_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName())) {             //if statement to check if the mentioned ID and is present in the file
        std::cout << std::endl << "Enter purchase quantity: " << "\n";
        int quantity;
        std::cin >> quantity;
        if (quantity <= store_obj.getQuantity()) {   //if statement to check if the mentioned quantity is less than the available stock
          store_obj.setQuantity(store_obj.getQuantity() - quantity);
          unsigned long int position = 0;
          position = -1 * sizeof(Store);  //to seek through the line to find the required item or its attributes
          book_file.seekp(position, std::ios::cur);
          book_file.write((char *) &store_obj, sizeof(store_obj));  //changes the quantity
          flag = 1;
          Sale sale_obj;
          std::ofstream file1("sales_information.txt", std::ios::binary | std::ios::app);  //opens a new text file and writes the sold item in it
          if (!file1) {
            std::cout << "FILE NOT FOUND\n";
            return 1;
          }
          sale_obj.set_sales_data(store_obj, quantity);
          file1.write((char*)&sale_obj, sizeof(sale_obj));
          file1.close();
          book_file.close();
          std::cout << "\nSale successful\n";
        } 
        else 
        {
          std::cout << "The required quantity is not available" << std::endl;
          flag = 1;
        }

      break;
      }
      }
      if(flag == 0)
      {
        book_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;

      }
        book_file.close();
    }
    break;
    default:   //if the selection is out of scope
      std::cout << "Invalid selection\n";
  }
return 1;
}

/*
 function to add new items to the products respective file by selectiong the product from the given sub menu
*/
int addNewItem() {
    std::cout << "\n\nADD SUB-MENU: " << std::endl;
    std::cout << "1) CD\n";
    std::cout << "2) DVD\n";
    std::cout << "3) Magazine\n";
    std::cout << "4) Book\n";
    int type;
    std::cin >> type;        //type of product
    switch(type)
    {
      case 1: { 
        Cd cd_obj;     
        std::ofstream file("cd.txt", std::ios::binary | std::ios::app);   //open cd file to store the data
        if (!file) {
          std::cout << "FILE NOT FOUND";
          return 1;
          }
        cd_obj.Output();          //calling the virtual function to accept user data and write it to the object 
        file.write((char*)&cd_obj, sizeof(cd_obj));        //write the object to file
        file.close();
        }
      break;
      case 2: {
        Dvd dvd_obj;
        std::ofstream file("dvd.txt", std::ios::binary|std::ios::app); //open dvd file to store the data
        if (!file) {
          std::cout << "FILE NOT FOUND";
          return 1;
          }
        dvd_obj.Output();   //calling the virtual function to accept user data and write it to the object 
        file.write((char*)&dvd_obj, sizeof(dvd_obj));  //write the object to file
        file.close();
        }
      break;
      case 3:{
        Magazines magazine_obj;
        std:: ofstream file("magazine.txt", std::ios::binary|std::ios::app); //open magazine file to store the data
        if (!file) {
        std::cout << "FILE NOT FOUND\n";
        return 1;
        }
        magazine_obj.Output();  //calling the virtual function to accept user data and write it to the object 
        file.write((char*) &magazine_obj, sizeof(magazine_obj)); //write the object to file
        file.close();
        }
      break;
      case 4: {
        Books book_obj;
        std::ofstream file("book.txt", std::ios::binary|std::ios::app); //open book file to store the data
        if (!file) {
          std::cout << "FILE NOT FOUND\n";
          return 1;
          }
        book_obj.Output();   //calling the virtual function to accept user data and write it to the object 
        file.write((char *) &book_obj, sizeof(book_obj));   //write the object to file
        file.close();
        }
      break;
      default:
      std::cout<<"Invalid selection";
    }
    return 1;
}

/*
 function to update the quantity (reduce or increase) and make the according changes in the file
*/
int updateStock()
{ 
  Store store_obj;
  int flag= 0;
  std::cout << "\n\nUPDATE SUB-MENU: " << std::endl;
  std::cout << "1) CDs\n";
  std::cout << "2) DVDs\n";
  std::cout << "3) Magazines\n";
  std::cout << "4) Books\n";
  int type;
  std::cin >> type;    //type of product
  switch(type) {
    case 1: { 
      std::fstream cd_file;
      std::cout << "\nEnter CD id: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      cd_file.open("cd.txt", std::ios::in | std::ios::out);          //open the cd.txt file in default mode
      while (cd_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName()))             //if statement to check if the mentioned ID and is present in the file
          {
            std::cout << "\n" << "Enter quantity: \n";
            int quantity;
            std::cin >> quantity;
            store_obj.setQuantity(quantity);
            unsigned long int position = 0;
            position = -1*sizeof(store_obj);       
            cd_file.seekp(position,std::ios::cur);  //used to seek through the file to find the required data
            cd_file.write((char*)&store_obj,sizeof(store_obj));  //writing the new quantity to file
            flag = 1;
            std::cout << "\nUpdate successful\n";
            break;
          }
          
      }
      if(flag == 0)
      {
        cd_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
        
      }
      cd_file.close();
    }
    break;
    case 2:{
      std::fstream dvd_file;
      std::cout << "\nEnter DVD id: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      dvd_file.open("dvd.txt", std::ios::in | std::ios::out);          //open the dvd.txt file in default mode
      while (dvd_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName()))   //if statement to check if the mentioned ID and is present in the file
          {
            std::cout << "\n" << "Enter quantity: \n";
            int quantity;
            std::cin >> quantity;
            store_obj.setQuantity(quantity);
            unsigned long int position = 0;
            position = -1*sizeof(store_obj);
            dvd_file.seekp(position,std::ios::cur);    //used to seek through the file to find the required data
            dvd_file.write((char*)&store_obj,sizeof(store_obj));     //writing the new quantity to file
            flag = 1;
            std::cout << "\nUpdate successful\n";
            break;
          }
      }
      if(flag == 0)
      {
        dvd_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      dvd_file.close();
    }
    break;
    case 3:{
      std::fstream mag_file;
      std::cout << "\nEnter magazine id: \n";
      int id = -1;
      std::cin >> id;char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      mag_file.open("magazine.txt", std::ios::in | std::ios::out);          //open the magazine.txt file in default mode
      while (mag_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName()))             //if statement to check if the mentioned ID and is present in the file
        {
          std::cout << "\n" << "Enter quantity: \n";
          int quantity;
          std::cin >> quantity;
          store_obj.setQuantity( quantity );
          unsigned long int position = 0;
          position = -1*sizeof(store_obj);
          mag_file.seekp(position,std::ios::cur);     //used to seek through the file to find the required data
          mag_file.write((char*)&store_obj,sizeof(store_obj));    //writing the new quantity to file
          flag = 1;
          std::cout << "\nUpdate successful\n";
          break;
        }
      }
      if(flag == 0)
      {
        mag_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      mag_file.close();
    }
    break;
    case 4:{
      std::fstream book_file;
      std::cout << "\nEnter books id: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      book_file.open("book.txt", std::ios::in | std::ios::out);          //open the book.txt file in default mode
      while (book_file.read((char *) &store_obj, sizeof(store_obj))) {    //read through all the items
      if ((id == store_obj.getId()) && (name == store_obj.getProductName()))             //if statement to check if the mentioned ID and is present in the file
        {
          std::cout<< "\n" << "Enter quantity: \n";
          int quantity;
          std::cin >> quantity;
          store_obj.setQuantity(quantity);
          unsigned long int position = 0;
          position = -1*sizeof(store_obj);
          book_file.seekp(position,std::ios::cur);    //used to seek through the file to find the required data
          book_file.write((char*)&store_obj,sizeof(store_obj));  //writing the new quantity to file
          flag = 1;
          std::cout << "\nUpdate successful\n";
          break;
        }
      }
      if(flag == 0)
      {
        book_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      book_file.close();
    }
    break;
    default:
    std::cout << "Invalid selection\n";
  }
return 1;
}

/*
 Function to increase the quantity and make the necessary changes in the file
*/
int restockItems()
{ 
  Store store;
  int flag= 0;
  std::cout << "\n\nRESTOCK SUB-MENU: " << std::endl;
  std::cout << "1) CDs\n";
  std::cout << "2) DVDs\n";
  std::cout << "3) Magazines\n";
  std::cout << "4) Books\n";
  int type;
  std::cin >> type;     //type of product
  switch(type) {
    case 1: {
      std::fstream cd_file;
      std::cout << "\nEnter ID: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      cd_file.open("cd.txt", std::ios::in | std::ios::out);          //open the cd.txt file in default mode
      while (cd_file.read((char *) &store, sizeof(store))) {    //read through all the items
      if ((id == store.getId()) && (name == store.getProductName())) {     //if statement to check if the mentioned ID and is present in the file
        std::cout << "\n" <<"Enter quantity: \n";
        int quantity;
        std::cin >> quantity;
        store.setQuantity(store.getQuantity() + quantity);    //increase the available quantity with the user provided quantity
        unsigned long int position = 0;
        position = -1 * sizeof(store);
        cd_file.seekp(position, std::ios::cur);        //to seek throught the file to find the data
        cd_file.write((char*)&store, sizeof(store));     //write the new changes to file
        flag = 1;
        std::cout << "\nCD updated\n";
        break;
      }
      }
      if (flag == 0) {
        cd_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      cd_file.close();
    }
    break;
    case 2:
    {
      std::fstream dvd_file;
      std::cout << "\nEnter ID: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      dvd_file.open("dvd.txt", std::ios::in | std::ios::out);          //open the dvd.txt file in default mode
      while (dvd_file.read((char *) &store, sizeof(store))) {    //read through all the items
        if ((id == store.getId()) && (name == store.getProductName())) {     //if statement to check if the mentioned ID and is present in the file
          std::cout << "\nEnter quantity: \n";
          int quantity;
          std::cin >> quantity;
          store.setQuantity(store.getQuantity() + quantity);    //increase the available quantity with the user provided quantity
          unsigned long int position = 0;
          position = -1 * sizeof(store);
          dvd_file.seekp(position, std::ios::cur);    //to seek throught the file to find the data
          dvd_file.write((char*)&store, sizeof(store));   //write the new changes to file
          flag = 1;
          std::cout << "\nDVD updated\n";
          break;
        }
      }
      if (flag == 0) {
        dvd_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      dvd_file.close();
    }
    break;
    case 3: {
      std::fstream mag_file;
      std::cout << "\nEnter ID: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      mag_file.open("magazine.txt", std::ios::in | std::ios::out);          //open the magazine.txt file in default mode
      while (mag_file.read((char *) &store, sizeof(store))) {    //read through all the items
        if ((id == store.getId()) && (name == store.getProductName())) {     //if statement to check if the mentioned ID and is present in the file
          std::cout << "\nEnter quantity: \n";
          int quantity;
          std::cin >> quantity;
          store.setQuantity(store.getQuantity() + quantity);     //increase the available quantity with the user provided quantity
          unsigned long int position = 0;
          position = -1 * sizeof(store);
          mag_file.seekp(position, std::ios::cur);     //to seek throught the file to find the data
          mag_file.write((char*)&store, sizeof(store));   //write the new changes to file
          flag = 1;
          std::cout << "\nMagazine updated\n";
          break;
        }
      }
      if (flag == 0) {
        mag_file.close();
        std::cout << "\nProduct doesn't exist" << std::endl;
      }
      mag_file.close();
    }
    break;
    case 4:
    {
      std::fstream book_file;
      std::cout << "\nEnter ID: \n";
      int id = -1;
      std::cin >> id;
      char name[20];
      std::cout << "\nEnter name: \n";
      std::cin.ignore();   //is used to ignore or clear one or more characters from the input buffer.
      std::cin.getline(name, 20);
      book_file.open("book.txt", std::ios::in | std::ios::out);          //open the book.txt file in default mode
      while (book_file.read((char *) &store, sizeof(store))) {    //read through all the items
        if ((id == store.getId()) && (name == store.getProductName())) {     //if statement to check if the mentioned ID and is present in the file
          std::cout << "\nEnter quantity: \n";
          int quantity;
          std::cin >> quantity;
          store.setQuantity(store.getQuantity() + quantity);    //increase the available quantity with the user provided quantity
          unsigned long int position = 0;
          position = -1 * sizeof(store);
          book_file.seekp(position, std::ios::cur);   //to seek throught the file to find the data
          book_file.write((char*)&store, sizeof(store));   //write the new changes to file
          flag = 1;
          std::cout << "\nBOOK updated\n";
          break;
        }
      }
      if(flag == 0)
      {
        book_file.close();
        std:: cout << "\nProduct doesn't exist" << std::endl;
      }
      book_file.close();
    }
    break;
    default:
      std::cout << "Wrong choice!\n";
  }
return 1;
}

/*
 function used to print the report of sales on the screen
*/
int viewreport()
{ 
  Sale sale;     //an object of Sale class
  std:: ifstream infile("sales_information.txt");   //opening sales information data
  if(!infile)
  {
    std::cout << "FILE NOT FOUND\n";
    return 1;
  }
  std::cout <<"\n\n\t\t\t---------------------------------------------";
  std::cout << "\n\t\t\t\tSALES REPORT INFO \n\t\t\t---------------------------------------------" << std::endl << "\n";
  while(infile.read((char*)&sale,sizeof(sale)))   //read through the file
  {
    sale.display_info();    //calling the function to display report of sales
  }
  infile.close();
  return 1;
}

/*
 Main function to display the main menu and run the required functions like sellItems(),restockItems(), addNewItems(); 
*/
TEST_CASE("testing", "[testing]") {
         
          REQUIRE(addNewItem() == 1);

          REQUIRE(sellItems() ==1 );
    
          REQUIRE(restockItems() ==1 );
         
          REQUIRE(updateStock() == 1);
       
          REQUIRE(viewreport() ==1 );
         
        
}
