#ifndef _CD_H_
#define _CD_H_
#include "Store.h"
#include <iostream>
/*
Cd.h
MISIS: M00734828
Created: 5/1/2021
Updated: 15/1/2021
*/

/*
 Cd is a sublcass of Store
*/
class Cd: public Store
{
  char type[20];
public:

//function used to take the type of cd from user
void Output()
{   
  Store::Output();
  std::cout << "Enter the type of DVD: ";
  std::cin.ignore();
  std::cin.getline(type, 20);
}

};

#endif