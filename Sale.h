#ifndef _SALE_H_
#define _SALE_H_
#include "Store.h"
#include <ctime>
/*
Sale.h
MISIS: M00734828
Created: 9/1/2021
Updated: 15/1/2021
*/

/*
 Sale class used to print the report of sales
*/
class Sale
{
private:
  time_t sale_date_time;  //to get the system time
  int sales_quantity;   
  Store store;  //object of Store class
public:
  void set_sales_data(Store it, int item_quantity);
  void display_info();
};
#endif