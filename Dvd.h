#ifndef _DVD_H_
#define _DVD_H_
#include <iostream>
#include "Store.h"
/*
Dvd.h
MISIS: M00734828
Created: 4/1/2021
Updated: 15/1/2021
*/

/* 
 Dvd is a sublcass of Store
*/
class Dvd: public Store
{
  char type[20]; 
public:

//function used to take the type of dvd from user
void Output()
{
  Store::Output();
  std::cout << "Enter the type of DVD: ";
  std::cin.ignore();
  std::cin.getline(type, 20);
}
};
#endif