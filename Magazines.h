#ifndef _BOOKS_H_
#define _BOOKS_H_
#include<iostream>
#include "Store.h"
/*
Magazines.h
MISIS: M00734828
Created: 4/1/2021
Updated: 15/1/2021
*/

/*
 Magazine is a subclass of Store
*/
class Magazines: public Store
{
  char category[20];
  char title[20];
public:

/*
 virtual functional used to take category and title from the user
*/
void Output()
  {
    Store::Output(); 
    std::cout << "Enter the Category: ";
    std::cin.ignore();
    std::cin.getline(category, 20);
    std::cout << "Enter the Title: ";
    std::cin.getline(title, 20);
  }
  
};
#endif