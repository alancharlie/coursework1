#ifndef _STORE_H_
#define _STORE_H_
#include <iostream>
/*
Store.h
MISIS: M00734828
Created: 4/1/2021
Updated: 15/1/2021
*/

// Base class for the specified products
class Store
{
protected:
  char product_name[20]; 
  int product_id;
  int quantity;
  double price;
public:
  std::string getProductName();
  int getId();
  double getPrice();
  void setQuantity(int quantity);
  int getQuantity();
  /*
    virtual function to accept values from the user.
  */
  virtual void Output()
  { 
  std::cout << "Enter the product ID: ";
  std::cin >> product_id;
  std::cout << "Enter the name of the product: ";
  std::cin.ignore();
  std::cin.getline(product_name,20);
  std::cout << "Enter the product quantity: ";
  std::cin >> quantity;
  std::cout << "Enter the product price: ";
  std::cin >> price;
  }
};
#endif