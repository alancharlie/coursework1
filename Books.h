#ifndef _BOOK_H_
#define _BOOK_H_
#include<iostream>
#include "Store.h"
/*
Books.h
MISIS: M00734828
Created: 5/1/2021
Updated: 15/1/2021
*/

//Books is a sublcass of Store
class Books: public Store
{
  char author[20];
  char edition[20];
public:

//function used to take the author and edition of books from user
void Output()
{
  Store::Output();
  std::cout << "Enter the author: ";
  std::cin.ignore();
  std::cin.getline(author, 20);
  std::cout << "Enter the Edition: ";
  std::cin.getline(edition, 20);
}

};
#endif